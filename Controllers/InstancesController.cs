using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Controllers
{
    public class InstancesController : Controller
    {
        public IEnumerable<InstanceModel> Index()
        {
            return new[]{
                new InstanceModel {
                    Id = Guid.NewGuid(),
                    OrganizationName = "OLOLO",
                    OrganizationId = Guid.NewGuid()
                },
                new InstanceModel {
                    Id = Guid.NewGuid(),
                    OrganizationName = "OLOLO2",
                    OrganizationId = Guid.NewGuid(),
                }
            };
        }
    }

    public class InstanceModel
    {
        public Guid Id { get; set; }
        public string OrganizationName { get; set; }
        public Guid OrganizationId { get; set; }
        public Guid TenantProductId {get;set;} = Guid.NewGuid();
        public Guid TenantId {get;set;} = Guid.NewGuid();
        public string RSOVersion {get;set;} = "123";
        public string CRMVersion {get;set;} = "123";
        public string Status {get;set;} = "FFFFUUUUUUU";

    }
}