﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Configuration;

namespace react_deployment
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public IConfigurationRoot Configuration { get; set; }
        public Startup(IHostingEnvironment env)
        {
            // Configuration = new ConfigurationBuilder()
            //     .SetBasePath(env.ContentRootPath)
            //     .AddJsonFile("config.json")
            //     .AddJsonFile("appsettings.json")
            //     .Build();
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            
            // services
            //     .AddAuthentication(opt => {
            //         opt.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //         opt.DefaultScheme = OpenIdConnectDefaults.AuthenticationScheme;
            //     })
            //     .AddCookie()
            //     .AddOpenIdConnect(option => {
            //         option.ClientId = Configuration["AzureAD:ClientId"];
            //         option.Authority = String.Format(Configuration["AzureAd:AadInstance"], Configuration["AzureAd:Tenant"]);
            //         option.SignedOutRedirectUri = Configuration["AzureAd:PostLogoutRedirectUri"];
            //         option.Events = new OpenIdConnectEvents
            //         {
            //             OnRemoteFailure = OnAuthenticationFailed,
            //         };
            //     });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMvc(routeBuilder =>{
                routeBuilder.MapRoute("DefaultRoute", "app/{controller=Instances}/{action=Index}");
            });
            
            app.UseStatusCodePagesWithReExecute("/");
            app.UseDefaultFiles();
            app.UseStaticFiles();
            
        }
    }
}
