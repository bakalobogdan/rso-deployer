import * as React from "react";
import { Redirect } from "react-router-dom";
import AuthContext from "./common/authentication";
import AdminApp from "./admin-app";
import { Login } from "./views/login";

const AccessControl:React.SFC = (props) => {
    let auth:AuthContext = AuthContext.getInstance();
    if(auth.isAuthenticated()) {
        if (auth.isInRole("admin")) {
            return <AdminApp />;
        } else {
            return <div>WAT?</div>;
        }
    } else {
        return <Redirect exact to="/login" component={ Login } />;
    }
};

export default AccessControl;