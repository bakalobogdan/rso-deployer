import * as React from "react";
import { browserHistory } from "react-router";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Sidebar, SideBarButton } from "./components/sidebar";
import { Header } from "./components/header";
import { Dashboard } from "./views/dashboard";
import { Instances } from "./views/instances";
import { Servers } from "./views/servers";
import { RSOTemplates } from "./views/templates";
import { RSOVersions } from "./views/versions";
import { createHashHistory } from "history";


export default class AdminApp extends React.Component<any, any> {
    render(): any {
        return (
            <Router>
                <div className="wrapper">
                    <Header logoText="RSO Deployment Tool"></Header>
                    <Sidebar>
                        <SideBarButton route="/instances" text="Instances" icon="fa fa-cubes" />
                        <SideBarButton route="/servers" text="Servers" icon="fa fa-hdd-o" />
                        <SideBarButton route="/templates" text="RMA Templates" icon="fa fa-gear" />
                        <SideBarButton route="/versions" text="RSO Versions" icon="fa fa-code-fork" />
                    </Sidebar>
                    <div className="main">
                        <Route exact path="/" component={Dashboard} />
                        <Route path="/instances" component={Instances} />
                        <Route path="/servers" component={Servers} />
                        <Route path="/templates" component={RSOTemplates} />
                        <Route path="/versions" component={RSOVersions} />
                    </div>
                </div>
            </Router>
        );
    }
}