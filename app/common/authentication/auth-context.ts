import * as AuthenticationContext from "adal-angular";
export interface IAuthContext {
    isAuthenticated: () => boolean;
    isInRole:(role:string) => boolean;
}
export default class AuthContext implements IAuthContext {
    private static _instance: AuthContext = new AuthContext();
    static getInstance: ()=> AuthContext = ()=> {
        return AuthContext._instance;
    }
    private _authContext: any;
    private constructor() {
        if(!AuthContext._instance) {
            AuthContext._instance = this;
        }
        this._authContext = new AuthenticationContext({
            instance: "https://login.microsoftonline.com/",
            tenant: "123960e9-3ad9-49de-88e7-7e2416d11b55",
            // the client ID of the app from the Azure Portal
            clientId: "821e1da0-dd89-4a83-a76d-0fcbf8085639",
            // where do we want to go after logging out
            postLogoutRedirectUri: window.location.origin,
            cacheLocation: "localStorage",
            redirectUri: "http://localhost:5000/login"
        });
    }
    isAuthenticated():boolean {
        if(this._authContext.getCachedUser()) {
            return true;
        } else {
            return false;
        }
    }
    isInRole(role: string):boolean {
        let result: boolean = false;
        // todo: remove
        if(this.isAuthenticated()) {
            let user:any = this._authContext.getCachedUser();
            if(user && user.profile && user.profile.roles) {
                result = (user.profile.roles as Array<any>).indexOf(role) > -1;
            }
        }
        return result;
    }
    handleRedirect(): void {
        if(this._authContext.isCallback(window.location.hash)) {
            this._authContext.handleWindowCallback();
        }
    }
    isCallback(): boolean {
        return this._authContext.isCallback(window.location.hash);
    }
    login():any {
        this._authContext.login();
    }
    logout():any {
        this._authContext.logout();
    }
}
