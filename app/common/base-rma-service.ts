import AuthContext from "adal-angular";
import axios, { AxiosInstance, AxiosStatic } from "axios";

var authContext: any = new AuthContext({
    tenant: "tenantId",
    clientId: "clientId",
    redirectUrl: "redirectUrl",
    cacheLocation: "localStorage",
    postLogoutRedirectUri: window.location.origin
});

export default class BaseRMAService {
    /**
     * Initializes default headers for all requests to our API
     */
    constructor() {
        this.axios = axios;
    }
    private readonly Authenticate: any = () => {
        console.log("ololo");
    }
    private readonly DefaultHeaders: Headers;
    private readonly axios: AxiosStatic;
}