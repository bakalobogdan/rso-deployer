import * as React from "react";

export interface ICommandBarButton {
    text: string;
    onClick: (event: any) => any;
    icon: string;
}

export const CommandBarButton: React.SFC<ICommandBarButton> = (props) => {
    return (
        <button onClick={props.onClick}>
            <i className={props.icon} aria-hidden="true"></i>
            {props.text}
        </button>
    );
};