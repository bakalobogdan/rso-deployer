import * as React from "react";
import "./command-bar.scss";

export const CommandBar: React.SFC = (props) => {
    return (
        <div className="command-bar">
            {props.children}
        </div>
    );
};
