import * as React from "react";
import * as ReactDOM from "react-dom";

import {
    Link
} from "react-router-dom";

import "./header.scss";

export interface IHeaderProps { logoText: string; }

export const Header: React.SFC<IHeaderProps> = (props) => {
    return (
        <header className="header">
            <div className="logo">
                <Link to="/">{props.logoText}</Link>
            </div>
        </header>
    );
};