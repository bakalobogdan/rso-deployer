// import * as React from "react";
// import { Route, Redirect } from "react-router";
// import AuthContext from "../../common/authentication/auth-context";

// export interface IPrivateRouteProps {
//     component: React.Component;
//     role: string;
//     [extraProps: string]: any;
// }

// const PrivateRoute: any = ({ component: Component, ...rest }) => (
//     <Route {...rest} render={ props => {
//         let isAuthenticated:boolean = AuthContext.getInstance().isAuthenticated();
//         return isAuthenticated ? (
//             <Component {...props} />
//         ) : (
//             <Redirect to={{
//                 pathname: "/login",
//                 state: { from: props.location }
//               }}/>
//         );}
//     } />);

// export default PrivateRoute;