import * as React from "react";
import * as ReactDOM from "react-dom";
import {
    BrowserRouter as Router,
    Route,
    Link
  } from "react-router-dom";

import "./sidebar.scss";

export class SideBarButton extends React.Component<ISideButtonProps> {
    render(): any {
        return (
            <Link to={this.props.route}>
                <i className={this.props.icon}></i>
                <span>
                    {this.props.text}
                </span>
            </Link>
        );
    }
}

export interface ISideButtonProps {
    route: string;
    icon: string;
    text: string;
}

export interface ISidebarState {
    isSmallSideBar: boolean;
}

export class Sidebar extends React.Component<any, ISidebarState> {
    constructor() {
        super();
        this.state = {
            isSmallSideBar: false
        };
    }
    toggleSmallBar = () => {
        this.setState({
            isSmallSideBar: !this.state.isSmallSideBar
        });
    }
    render(): any {
        return (
        <div className={"sidebar" + (this.state.isSmallSideBar ? " small" : "")}>
            <nav>
                <a onClick={this.toggleSmallBar}><i className="fa fa-bars"></i></a>
                {this.props.children}
            </nav>
        </div>);
    }
}