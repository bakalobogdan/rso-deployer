import * as React from "react";
import { Link } from "react-router-dom";

import ReactTable from "react-table";

import "./window.scss";

export const Window: React.SFC<any> = (props) => {
    // tslint:disable-next-line:typedef
    return (
        <section className="window">
            <header className="window-header">
                <span className="window-header__headline">
                    {props.headerText}
                </span>
                <Link to="/" className="window-header__close-btn">
                    <i className="fa fa-close fa-1x"></i>
                </Link>
            </header>
            <main className="window-body">
                {props.children}
            </main>
        </section>
    );
};