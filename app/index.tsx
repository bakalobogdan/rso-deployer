import * as React from "react";
import * as ReactDOM from "react-dom";
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect
} from "react-router-dom";


import "./styles/common.scss";
import "font-awesome-webpack";
import "../node_modules/react-table/react-table.css";

import { createStore, applyMiddleware, Store, Dispatch, Unsubscribe, AnyAction, Reducer } from "redux";
import { Provider } from "react-redux";
import * as promiseMiddleware from "redux-promise";
import AuthContext from "./common/authentication/auth-context";
import AdminApp from "./admin-app";
import AccessControl from "./access-control";
import { Login } from "./views/login/login";

let reducer: Reducer<any> = (s:any, a: AnyAction) => {
    switch(a.type) {
        default:
            let newState: any = {};
            return newState;
    }
};

const createStoreWithMiddleware: any = createStore(reducer, {}, applyMiddleware(promiseMiddleware));



ReactDOM.render(
     <Provider store={createStoreWithMiddleware}>
        <Router>
            <div>
                <Route path="/" component={ AccessControl }  />
                <Route exact path="/login" component={ Login } />
            </div>
        </Router>
    </Provider>
    ,
    document.getElementById("container")
);