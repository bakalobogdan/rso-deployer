export const FETCH_INSTANCES: string = "FETCH_INSTANCES";
export const DELETE_INSTANCE: string = "DELETE_INSTANCE";
export const ROLLOVER_INSTANCE: string = "ROLLOVER_INSTANCE";
export const ADJUST_TEMPLATE: string = "ADJUST_TEMPLATE";

export const BULK_DELETE_INSTANCES: string = "BULK_DELETE_INSTANCES";
export const BULK_ROLLOVER_INSTANCES: string = "BULK_ROLLOVER_INSTANCES";

import * as React from "react";
import { AnyAction } from "redux";

interface IPromiseAction extends AnyAction { }

interface IFetchInstancesPayload {
    readonly request: Promise<Response>;
}

export function fetchInstances(pageSize: number, pageNumber: number, searchString: string): IPromiseAction {
    console.log("fetch instances from our API");
    const request: Promise<Response> = fetch("/app/instances");
    return {
        type: FETCH_INSTANCES,
        payload: request
    };
}

export function deleteInstance(instanceId: string): void {
    console.log("delete instance action");
}

export function rolloverInstance(instanceId: string): void {
    console.log("rollover instance action");
}