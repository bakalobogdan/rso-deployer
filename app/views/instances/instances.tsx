import * as React from "react";
import * as ReactDOM from "react-dom";
import ReactTable from "react-table";
import "react-table/react-table.css";
import "./instances.scss";
import { CommandBar, CommandBarButton } from "../../components/command-bar";
import { Window } from "../../components/window/window";

import { IInstanceModel } from "./services";

export class InstancesProps {
    instances: Array<IInstanceModel>;
}

export class Instances extends React.Component<InstancesProps, InstancesProps> {
    constructor(props: any) {
        super(props);
        this.state = {instances: [
            {
                id: "1",
                organization: "org",
                tenantId: "1",
                tenantProductId:"1",
                rsoVersion: "1",
                crmVersion: "1",
                status: "1"
            },
            {
                id: "2",
                organization: "org",
                tenantId: "2",
                tenantProductId:"2",
                rsoVersion: "2",
                crmVersion: "2",
                status: "2"
            }
        ]};
    }
    // tslint:disable-next-line:typedef
    render() {
        return (
            <Window className="instances" headerText="Instances">
                <CommandBar>
                    <CommandBarButton onClick={(x)=> x} icon="fa fa-briefcase fa-3" text="Rollover" />
                    <CommandBarButton onClick={(x)=> x} icon="fa fa-trash fa-3" text="Delete" />
                    <CommandBarButton onClick={(x)=> x} icon="fa fa-refresh fa-3" text="Refresh" />
                    <CommandBarButton onClick={(x)=> x} icon="fa fa-columns fa-3" text="Columns" />
                </CommandBar>
                <ReactTable
                    data={this.state.instances}
                    columns={[
                        {
                            Header: "Organization",
                            accessor: d=> d.organization,
                            id: "organization"
                        },
                        {
                            Header: "Tenant Id",
                            accessor: d=> d.tenantId,
                            id: "tenantId"
                        },
                        {
                            Header: "Tenant Product Id",
                            accessor: d=> d.tenantProductId,
                            id: "tenantProductId"
                        },
                        {
                            Header: "RSO Version",
                            accessor: d=> d.rsoVersion,
                            id: "rsoVersion"
                        },
                        {
                            Header: "CRM Version",
                            accessor: d=> d.crmVersion,
                            id: "crmVersion"
                        },
                        {
                            Header: "Status",
                            accessor: d=> d.status,
                            id: "status"
                        }
                    ]}
                >
                </ReactTable>
            </Window>
        );
    }
}