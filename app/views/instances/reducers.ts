import { Reducer, AnyAction, combineReducers } from "redux";
export interface IAnyExtra {
    [extraProps: string]: any;
}
export interface IInstancesState extends IAnyExtra {
    ololo: boolean;
}

const InstancesReducer: Reducer<any> = function (s: IInstancesState = { ololo: false }, a: AnyAction): IInstancesState {
    switch(a.type) {
        default:
            return {...s, ololo: true };
    }
};

export default combineReducers({ InstancesReducer });