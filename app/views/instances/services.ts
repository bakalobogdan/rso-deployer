export interface IInstanceModel {
    id: string;
    organization: string;
    tenantId: string;
    tenantProductId:string;
    rsoVersion: string;
    crmVersion: string;
    status: string;
}
export interface IPagedResponse<T> {
    page: number;
    pageSize: number;
    items: Array<T>;
}
export interface IPagedRequest {
    page: number;
    pageSize: number;
    searchString: string;
    [extraProps: string]: any;
}


export class InstancesService {
    constructor() {
        
    }
    getInstances(data: any): Promise<Response> {
        let { page, pageSize, searchString } = data;
        return fetch(`api/instances?page=${page}&pageSize=${pageSize}&searchString=${searchString}`);
    }

}