import * as React from "react";
import AuthContext from "../../common/authentication/auth-context";
import { Redirect, withRouter } from "react-router-dom";

export const Login:any = (props) => {
    let authCtx: AuthContext = AuthContext.getInstance();
    if(authCtx.isCallback()) {
        authCtx.handleRedirect();
    }
    if (authCtx.isAuthenticated()) {
        return <Redirect to="/" />;
    } else {
        authCtx.login();
    }
    return <div></div>;
};