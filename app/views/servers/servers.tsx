import * as React from "react";
import ReactTable from "react-table";
import { CommandBar, CommandBarButton } from "../../components/command-bar";
import { Window } from "../../components/window/window";

export class ServerModel {
    subscriptionId: string;
    tenantId: string;
    sqlServerName: string;
    resourceGroupName: string;
    adminUserName: string;
    dtuCapacity: string;
    dtuUtilization: string;
    pwdRolloverDate: string;
    rolloverStatus: string;
}
export class ServersState {
    servers: Array<ServerModel>;
}

export class Servers extends React.Component<any, ServersState> {
    constructor(props: any) {
        super(props);
        this.state = {
            servers: []
        };
    }
    // tslint:disable-next-line:typedef
    render() {
        return (
            <Window className="servers" headerText="Servers">
                <CommandBar>
                    <CommandBarButton onClick={(x)=> x} icon="fa fa-plus fa-3" text="Add" />
                    <CommandBarButton onClick={(x)=> x} icon="fa fa-refresh fa-3" text="Refresh" />
                    <CommandBarButton onClick={(x)=> x} icon="fa fa-columns fa-3" text="Columns" />
                </CommandBar>
                <ReactTable
                    data={this.state.servers}
                    columns={[
                        {
                            Header: "Subscription ID",
                            accessor: d=> d.subscriptionId,
                            id: "subscriptionId"
                        },
                        {
                            Header: "Tenant Id",
                            accessor: d=> d.tenantId,
                            id: "tenantId"
                        },
                        {
                            Header: "Tenant Product Id",
                            accessor: d=> d.resourceGroupName,
                            id: "resourceGroupName"
                        },
                        {
                            Header: "RSO Version",
                            accessor: d=> d.dtuCapacity,
                            id: "dtuCapacity"
                        },
                        {
                            Header: "CRM Version",
                            accessor: d=> d.dtuUtilization,
                            id: "dtuUtilization"
                        },
                        {
                            Header: "Status",
                            accessor: d=> d.pwdRolloverDate,
                            id: "pwdRolloverDate"
                        },
                        {
                            Header: "Status",
                            accessor: d=> d.rolloverStatus,
                            id: "rolloverStatus"
                        }
                    ]}
                >
                </ReactTable>
            </Window>
        );
    }
}