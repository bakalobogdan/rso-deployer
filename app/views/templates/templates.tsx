import * as React from "react";
import ReactTable from "react-table";
import { CommandBar, CommandBarButton } from "../../components/command-bar";
import { Window } from "../../components/window/window";

class RSOTemplateModel {
    id: string;
    name: string;
    description: string;
    isCustom: boolean;
    minRSOVersion: string;
    maxRSOVersion: string;
}

interface IRSOTemplatesProps {
    templates: Array<RSOTemplateModel>;
}

class RSOTemplatesState {
    templates: Array<RSOTemplateModel>;
}

export class RSOTemplates extends React.Component<any, RSOTemplatesState> {
    constructor(props: any) {
        super(props);
        this.state = {
            templates: []
        };
    }
    // tslint:disable-next-line:typedef
    render() {
        return (
            <Window className="templates" headerText="RMA Templates">
                <CommandBar>
                    <CommandBarButton onClick={(x)=> alert("OLOLOLO")} icon="fa fa-plus fa-3" text="Add" />
                    <CommandBarButton onClick={(x)=> x} icon="fa fa-refresh fa-3" text="Refresh" />
                    <CommandBarButton onClick={(x)=> x} icon="fa fa-columns fa-3" text="Columns" />
                </CommandBar>
                <ReactTable
                    data={this.state.templates}
                    columns={[
                        {
                            Header: "Subscription ID",
                            accessor: d=> d.id,
                            id: "subscriptionId"
                        },
                        {
                            Header: "Tenant Id",
                            accessor: d=> d.name,
                            id: "tenantId"
                        },
                        {
                            Header: "Tenant Product Id",
                            accessor: d=> d.description,
                            id: "resourceGroupName"
                        },
                        {
                            Header: "RSO Version",
                            accessor: d=> d.isCustom,
                            id: "dtuCapacity"
                        },
                        {
                            Header: "CRM Version",
                            accessor: d=> d.minRSOVersion,
                            id: "dtuUtilization"
                        },
                        {
                            Header: "Status",
                            accessor: d=> d.maxRSOVersion,
                            id: "pwdRolloverDate"
                        }
                    ]}
                >
                </ReactTable>
            </Window>
        );
    }
}

function mapStateToProps(state: RSOTemplatesState): IRSOTemplatesProps {
    return {
        templates: state.templates
    };
}
