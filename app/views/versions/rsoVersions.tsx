import * as React from "react";
import { Window } from "../../components/window/window";
import { CommandBar, CommandBarButton } from "../../components/command-bar";

export class RSOVersions extends React.Component {
    render(): any {
        return (
            <Window headerText="RSO Versions">
                <CommandBar>
                    <CommandBarButton onClick={(x)=> x} icon="fa fa-plus fa-3" text="Add" />
                    <CommandBarButton onClick={(x)=> x} icon="fa fa-refresh fa-3" text="Refresh" />
                    <CommandBarButton onClick={(x)=> x} icon="fa fa-columns fa-3" text="Columns" />
                </CommandBar>
            </Window>
        );
    }
}